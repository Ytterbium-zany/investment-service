package com.punyisa.controller;

import com.punyisa.domain.InvestmentPlan;
import com.punyisa.domain.InvestmentPlanResponse;
import com.punyisa.service.InvestmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * GetInvestmentPlansController provides the api to calculate
 * compound interest of an investment
 * Created by Punyisa Kraisang on 9/16/2016.
 */
@RestController
public class GetInvestmentPlansController {

    @Autowired
    private InvestmentService investmentService;

    /**
     * Calculate compound interest for monthly investment.
     * Generate the result of the investment with monthly
     * compound interval.
     *
     * @param starting      the balance that is a base of investment.
     *                      Can be in any currency.
     * @param rate          the annual interest rate (%)
     * @param contribution  the money to be invested in each month.
     *                      Should be the same currency as starting.
     * @param period        the contribution period (month)
     * @return              the result of monthly investment calculation
     */
    @RequestMapping(value = "/plans", method = RequestMethod.GET)
    public @ResponseBody InvestmentPlanResponse getMonthlyInvestmentPlans(
            @RequestParam BigDecimal starting,
            @RequestParam BigDecimal rate,
            @RequestParam BigDecimal contribution,
            @RequestParam int period) {

        //convert annual interest rate (%) to monthly rate (decimal form)
        rate = rate.divide(new BigDecimal("12"), 6, RoundingMode.HALF_UP)
                .divide(new BigDecimal("100"));

        List<InvestmentPlan> investmentPlans = investmentService.getPlans(starting,rate,contribution, period);

        InvestmentPlanResponse response = new InvestmentPlanResponse();
        response.setStatusCode("0");
        response.setStatusMessage("Success");
        response.setInvestmentPlans(investmentPlans);

        return response;
    }

}
