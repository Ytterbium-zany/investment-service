package com.punyisa.domain;

import java.math.BigDecimal;

/**
 * Created by Punyisa Kraisang on 9/16/2016.
 */
public class InvestmentPlan {

    private int intervalNumber;
    private BigDecimal contribution;
    private BigDecimal interest;
    private BigDecimal accumulatedInterest;
    private BigDecimal accumulatedContribution;
    private BigDecimal accumulatedDeposit;
    private BigDecimal totalBalance;

    public int getIntervalNumber() {
        return intervalNumber;
    }

    public void setIntervalNumber(int intervalNumber) {
        this.intervalNumber = intervalNumber;
    }

    public BigDecimal getContribution() {
        return contribution;
    }

    public void setContribution(BigDecimal contribution) {
        this.contribution = contribution;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public BigDecimal getAccumulatedInterest() {
        return accumulatedInterest;
    }

    public void setAccumulatedInterest(BigDecimal accumulatedInterest) {
        this.accumulatedInterest = accumulatedInterest;
    }

    public BigDecimal getAccumulatedContribution() {
        return accumulatedContribution;
    }

    public void setAccumulatedContribution(BigDecimal accumulatedContribution) {
        this.accumulatedContribution = accumulatedContribution;
    }

    public BigDecimal getAccumulatedDeposit() {
        return accumulatedDeposit;
    }

    public void setAccumulatedDeposit(BigDecimal accumulatedDeposit) {
        this.accumulatedDeposit = accumulatedDeposit;
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }
}
