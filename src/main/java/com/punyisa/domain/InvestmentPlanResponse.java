package com.punyisa.domain;

import java.util.List;

/**
 * Created by Punyisa Kraisang on 9/16/2016.
 */
public class InvestmentPlanResponse {

    private String statusCode;
    private String statusMessage;
    private List<InvestmentPlan> investmentPlans;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<InvestmentPlan> getInvestmentPlans() {
        return investmentPlans;
    }

    public void setInvestmentPlans(List<InvestmentPlan> investmentPlans) {
        this.investmentPlans = investmentPlans;
    }
}
