package com.punyisa.service;

import com.punyisa.domain.InvestmentPlan;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * InvestmentService makes calculation of compound interest for investment
 * Created by Punyisa Kraisang on 9/16/2016.
 */
@Service
public class InvestmentService {

    /**
     * Generate the result of investment with a compound interval.
     *
     * @param starting      the balance that is a base of investment.
     *                      Can be in any currency.
     * @param rate          the interest rate (decimal form) of the compound interval
     * @param contribution  the money to be invested in each interval.
     *                      Should be the same currency as starting.
     * @param period        the number of compound interval
     * @return              the result of interval investment calculation
     */
    public List<InvestmentPlan> getPlans(BigDecimal starting, BigDecimal rate, BigDecimal contribution, int period) {

        BigDecimal currentBalance = starting;
        BigDecimal totalInterest = new BigDecimal("0");
        BigDecimal totalContribution = new BigDecimal("0");
        BigDecimal totalDeposit = starting;

        List<InvestmentPlan> investmentPlans = new ArrayList<>();
        for(int i=0; i<period; i++) {
            InvestmentPlan investmentPlan = new InvestmentPlan();
            investmentPlan.setIntervalNumber(i+1);
            investmentPlan.setContribution(contribution);

            currentBalance = currentBalance.add(contribution);

            BigDecimal interest = currentBalance.multiply(rate);
            investmentPlan.setInterest(interest);

            totalInterest = totalInterest.add(interest);
            investmentPlan.setAccumulatedInterest(totalInterest);

            totalContribution = totalContribution.add(contribution);
            investmentPlan.setAccumulatedContribution(totalContribution);

            totalDeposit = totalDeposit.add(contribution);
            investmentPlan.setAccumulatedDeposit(totalDeposit);

            currentBalance = currentBalance.add(interest);
            investmentPlan.setTotalBalance(currentBalance);

            investmentPlans.add(investmentPlan);
        }

        return investmentPlans;
    }

}
