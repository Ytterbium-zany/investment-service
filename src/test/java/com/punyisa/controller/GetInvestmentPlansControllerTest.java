package com.punyisa.controller;

import com.punyisa.domain.InvestmentPlanResponse;
import com.punyisa.service.InvestmentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Punyisa Kraisang on 9/16/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class GetInvestmentPlansControllerTest {

    @InjectMocks
    private GetInvestmentPlansController getInvestmentPlansController;

    @Mock
    private InvestmentService investmentServiceMock;

    @Test
    public void getMonthlyInvestmentPlans_receiveValidRequest_successWasReturned() {
        BigDecimal starting = new BigDecimal("5000");
        BigDecimal rate = new BigDecimal("10");
        BigDecimal contribution = new BigDecimal("10");
        int period = 6;

        InvestmentPlanResponse response = getInvestmentPlansController.getMonthlyInvestmentPlans(starting, rate, contribution, period);

        assertEquals("Success", response.getStatusMessage());
    }

    @Test
    public void getMonthlyInvestmentPlans_receiveValidRequest_getPlansWasCalledOnce() {
        BigDecimal starting = new BigDecimal("5000");
        BigDecimal annualRate = new BigDecimal("12");
        BigDecimal monthlyRate = annualRate.divide(new BigDecimal("12"), 6, RoundingMode.HALF_UP)
                .divide(new BigDecimal("100"));
        BigDecimal contribution = new BigDecimal("10");
        int period = 6;

        getInvestmentPlansController.getMonthlyInvestmentPlans(starting, annualRate, contribution, period);

        verify(investmentServiceMock, times(1)).getPlans(starting, monthlyRate, contribution, period);
    }
}
