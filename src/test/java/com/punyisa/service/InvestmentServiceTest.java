package com.punyisa.service;

import com.punyisa.domain.InvestmentPlan;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Punyisa Kraisang on 9/16/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class InvestmentServiceTest {

    @InjectMocks InvestmentService investmentService;

    @Test
    public void getPlans_periodIs0_investPlanIsReturnWithoutException() {
        BigDecimal starting = new BigDecimal("5000");
        BigDecimal rate = new BigDecimal("10");
        BigDecimal contribution = new BigDecimal("10");
        int period = 0;

        List<InvestmentPlan> plans = investmentService.getPlans(starting, rate, contribution, period);

        assertEquals(0, plans.size());
    }

    @Test
    public void getPlans_periodIsNot0_investPlanIsCalculated() {
        BigDecimal starting = new BigDecimal("5000");
        BigDecimal rate = new BigDecimal("0.01");
        BigDecimal contribution = new BigDecimal("10");
        int period = 1;

        List<InvestmentPlan> plans = investmentService.getPlans(starting, rate, contribution, period);

        assertEquals(1, plans.size());

        InvestmentPlan plan = plans.get(0);
        assertEquals(1, plan.getIntervalNumber());
        assertEquals(10, plan.getContribution().doubleValue(), 0.00001);
        assertEquals(10, plan.getAccumulatedContribution().doubleValue(), 0.00001);
        assertEquals(50.1, plan.getInterest().doubleValue(), 0.00001);
        assertEquals(50.1, plan.getAccumulatedInterest().doubleValue(), 0.00001);
        assertEquals(5010, plan.getAccumulatedDeposit().doubleValue(), 0.00001);
        assertEquals(5060.1, plan.getTotalBalance().doubleValue(), 0.00001);
    }

}
